function T = fkine(q, DH_para)
Alpha = DH_para.Alpha;
A = DH_para.A;
D = DH_para.D;
Theta = DH_para.Theta;
Sigma = DH_para.Sigma;
T = eye(4);
for i = 1:length(q)
    T = T * transformation_mdh(Alpha(i), A(i), D(i), Theta(i), Sigma(i), q(i));
end
