function q_min_index = inline_singularity_detect(qs, DH_para)

q = mod(abs(qs(:, 2)), pi);
for i = 1:length(q)
    if q(i) > pi - q(i)
        q(i) = pi - q(i);
    end
end
[q_min, q_min_index] = min(q);
